package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][]grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                grid[i][j] = initialState;
            }
        }

		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(numRows() < row && row < 0){
            throw new IndexOutOfBoundsException();
        }
        if(numColumns() < column && column < 0){
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
        // TODO Auto-generated method stub
        
    }

    @Override
    public CellState get(int row, int column) {
        if(numRows() < row && row < 0 ){
            throw new IndexOutOfBoundsException();
        }
        if (numColumns() < column && column < 0) {
            throw new IndexOutOfBoundsException();
        }

        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);

        for(int i = 0; i < numRows(); i++){
            for(int j = 0; j < numColumns(); j++){
                copyGrid.set(i, j, grid[i][j]);
            }
        } 
        // TODO Auto-generated method stub
        return copyGrid;
    }
    
}
